# Enable bitmap fonts

```
sudo mv /etc/fonts/conf.d/10-* /etc/fonts/conf.avail/
sudo mv /etc/fonts/conf.d/70-no-bitmaps.conf /etc/fonts/conf.avail/
```

# Copy fonts to local

```
cp fonts/* ~/.local/share/fonts/
```
